package game;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Player {
    private List<Training> trainingList = new LinkedList<Training>();

    public void addTraining(Training training) {
        trainingList.add(training);

    }

    public List<Training> getTrainingList() {
        return trainingList;
    }

    public void setTrainingList(List<Training> trainingList) {
        this.trainingList = trainingList;
    }

    public double getAverage() {
        final List<Integer> listTemp = new ArrayList<Integer>();
        trainingList.forEach(p -> listTemp.add(p.getTheBestOcena()));
        double sum = 0;
        for (Integer integer : listTemp) {
            sum += integer;
        }
        return sum / listTemp.size();
    }
}

