package game;

import java.util.ArrayList;
import java.util.List;

public class Training {

    private List<Integer> oceny = new ArrayList<Integer>();

    public void addOcena(Integer ocena) {
        oceny.add(ocena);
    }

    public List<Integer> getOceny() {
        return oceny;
    }

    public void setOceny(List<Integer> oceny) {
        this.oceny = oceny;
    }

    public int getTheBestOcena() {
        int max = 0;
        for (int i = 0; i < oceny.size(); i++) {
            if (oceny.get(i) > max)
                max = oceny.get(i);
        }
        return max;
    }

    public int getAverage() {
        int sum = 0;
        for (int i = 0; i < oceny.size(); i++) {
            sum += oceny.get(i);
        }
        return sum / oceny.size();
    }
}
