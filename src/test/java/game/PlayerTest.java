package game;

import org.junit.Assert;
import org.junit.Test;

public class PlayerTest {
    @Test
    public void createPlayerTest() {
        Player player = new Player();
        Assert.assertNotNull(player);
    }

    @Test
    public void addToTraining() {
        Player player = new Player();
        Training training = new Training();
        player.addTraining(training);
        Assert.assertTrue(player.getTrainingList().contains(training));
    }

    @Test
    public void getTraining() {
        Player player = new Player();
        Training training = new Training();
        player.addTraining(training);
        Assert.assertEquals(player.getTrainingList().get(0), training);
    }

    @Test
    public void getPlayerAverage() {
        Training t1 = new Training();
        Training t2 = new Training();
        Training t3 = new Training();


        t1.addOcena(10);
        t1.addOcena(4);

        t2.addOcena(6);
        t2.addOcena(4);

        t3.addOcena(8);
        t3.addOcena(14);

        Player p1 = new Player();
        p1.addTraining(t1);
        p1.addTraining(t2);
        p1.addTraining(t3);

        ;
        Assert.assertEquals(10.0, p1.getAverage(), 0.1);

    }

}
