package game;

import org.junit.Assert;
import org.junit.Test;

public class TrainingTest {
    @Test
    public void createTrainingTest() {
        Training t1 = new Training();
        Assert.assertNotNull(t1);
    }

    @Test
    public void checkOcenaTrainingTest() {
        Training t1 = new Training();
        t1.addOcena(5);
        Assert.assertTrue(t1.getOceny().contains(5));
    }

    @Test
    public void getTheBestOcena() {
        Training t = new Training();
        t.addOcena(5);
        t.addOcena(10);
        t.addOcena(2);
        t.addOcena(6);
        Assert.assertEquals(10, t.getTheBestOcena());
    }

    @Test
    public void getAverage() {
        Training t = new Training();
        t.addOcena(6);
        t.addOcena(10);
        t.addOcena(2);
        t.addOcena(6);
        Assert.assertEquals(6, t.getAverage());
    }

}
